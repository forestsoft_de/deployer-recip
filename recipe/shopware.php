<?php

namespace Deployer;

require_once 'recipe/common.php';

add(
    'shared_dirs',
    [
        '{{shopware_public_dir}}/media',
        '{{shopware_public_dir}}/files',
    ]
);
add(
    'create_shared_dirs',
    [
        '{{shopware_public_dir}}/media/archive',
        '{{shopware_public_dir}}/media/image',
        '{{shopware_public_dir}}/media/image/thumbnail',
        '{{shopware_public_dir}}/media/music',
        '{{shopware_public_dir}}/media/pdf',
        '{{shopware_public_dir}}/media/unknown',
        '{{shopware_public_dir}}/media/video',
        '{{shopware_public_dir}}/media/temp',
        '{{shopware_public_dir}}/files/documents',
        '{{shopware_public_dir}}/files/downloads',
        '{{shopware_public_dir}}/files/backup',
        '{{shopware_public_dir}}/files/backup/multi_edit',
    ]
);
add(
    'writable_dirs',
    [
        '{{shopware_public_dir}}/var/cache',
        '{{shopware_public_dir}}/web/cache',
        '{{shopware_public_dir}}/recovery',
        '{{shopware_public_dir}}/themes',
    ]
);
add(
    'executable_files',
    [
        '{{shopware_public_path}}/bin/console',
    ]
);
set('shopware_public_dir', 'src');
set('shopware_public_path', '{{release_path}}/{{shopware_public_dir}}');
set('shopware_temp_dir', '{{release_path}}/shopware-tmp');
set('shopware_temp_zip_file', '{{release_path}}/shopware.zip');
set('fcgi_sockets', []);

task('configure-env', function() {
    $hostname = get('shopware_account_host');

    $env = [
        'DB_HOST'           =>  get("app.mysql.host"),
        'DB_DATABASE'       =>  get("app.mysql.database"),
        'DB_USERNAME'       =>  get("app.mysql.user"),
        'DB_PASSWORD'       =>  get("app.mysql.password"),
        'DATABASE_URL'      => 'mysql://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}',
        'SHOP_URL'          => $hostname,
        'SHOPWARE_ENV'      => (stristr(get('app.base_url'), ".local")) ? 'dev' : 'production',
        'ACCOUNT_USER'      => get("shopware_account_user"),
        'ACCOUNT_PASSWORD'  => get("shopware_account_password")
    ];
    set('env', $env);
});


after("init", "configure-env");
task(
    'deploy:writable:create_dirs',
    function () {
        foreach (get('writable_dirs') as $dir) {
            run("cd {{release_path}} && mkdir -p $dir && sudo chown -R " . get("http_user") . " $dir && sudo chmod -R 775 $dir");
        }
    }
)->desc('Create required directories, configure via deploy.php');
before('deploy:writable', 'deploy:writable:create_dirs');

task(
    'deploy:shared:sub',
    function () {
        $sharedPath = "{{deploy_path}}/shared";
        foreach (get('create_shared_dirs') as $dir) {
            // Create shared dir if it does not exist.
            run("mkdir -p $sharedPath/$dir");
        }
    }
)->desc('Creating shared subdirs');
after('deploy:shared', 'deploy:shared:sub');

task(
    'kellerkinder:vendors',
    function () {

        run("cd {{release_path}} && touch src/.env");

        $env = get('env');
        foreach ($env as $var => $value) {
            run("cd {{release_path}} && echo \"" . $var . "=" . $value . "\" >> src/.env");
        }

        $dpendencies = [
            'k10r/deployment:1.2.0',
            'k10r/staging:1.0.3',
        ];

        run(
            "cd {{shopware_public_path}} && {{bin/composer}} update && {{bin/composer}} require --optimize-autoloader --prefer-dist --no-ansi --update-no-dev " . implode(" ", $dpendencies)
        );

    }
)->desc('Add required composer-based Shopware plugins for deployment')->setPrivate();

task('make-executeable', function() {
    $files = get('executable_files');
    if (count($files)) {
        foreach ($files as $file) {
            run("chmod 755 $file");
        }
    }
});
before('kellerkinder:vendors', 'make-executeable');
task(
    'deploy:vendors',
    function () {
        run('cd {{shopware_public_path}} && {{bin/composer}} {{composer_options}}');
    }
)->desc('Install composer dependencies');

task(
    'kellerkinder:shopware:plugins',
    function () {
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:plugin:refresh -q');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:plugin:reinstall K10rDeployment');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:plugin:deactivate SwagUpdate');

        foreach (get('plugins') as $plugin) {
            run(
                "cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:plugin:install --activate {$plugin}"
            );
        }
        $appConfig = get("app");

        if(!is_array($appConfig)) {
            DIE("could not read App config");
        }
        
        foreach ($appConfig["plugin_config"] as $pluginName => $pluginConfigs) {
            foreach ($pluginConfigs as $pluginConfig) {
                run(
                    sprintf(
                        "cd {{shopware_public_path}} && {{bin/php}} bin/console sw:plugin:config:set %s %s %s %s",
                        $pluginName,
                        $pluginConfig['name'],
                        $pluginConfig['value'],
                        isset($pluginConfig['shopId']) ? "--shop {$pluginConfig['shopId']}" : ""
                    )
                );
            }
        }
    }
)->desc('Install Shopware plugins after deployment. Configure plugins via deploy.php.');

task('read-plugins-from-composer', function() {

    $composerFile = tempnam("/tmp/","composer.json");
    download("{{shopware_public_path}}/composer.json", $composerFile);
    $json = json_decode(file_get_contents($composerFile), true);

    $config = (get("app"));

    $env = stristr($config["base_url"], ".local") ? 'dev' : 'production';

    $plugins = get("plugins");
    if (!is_array($plugins)) {
        $plugins = [];
    }
    
    if (isset($json["extra"]["plugins"][$env])) {
        $plugins = array_merge($plugins, array_keys($json["extra"]["plugins"][$env]));
    }

    set('plugins', $plugins);

    unlink($composerFile);

});

task(
    'kellerkinder:shopware:config',
    function () {
        //run("cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:snippets:update");
        foreach (get('shopware_config') as $config) {
            run(
                sprintf(
                    run("cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:config:set %s %s %s"),
                    $config['name'],
                    $config['value'],
                    isset($config['shopId']) ? "--shop {$config['shopId']}" : ""
                )
            );
        }
    }
)->desc('Update snippets and set Shopware core configuration');


task(
    'kellerkinder:shopware:theme',
    function () {
        run(
            "cd {{shopware_public_path}} && {{bin/php}} bin/console sw:theme:synchronize -q"
        );
        foreach (get('theme_config') as $themeName => $themeSettings) {
            foreach ($themeSettings as $setting) {
                run(
                    sprintf(
                        "cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:theme:update -q --theme %s --setting %s --value %s %s",
                        $themeName,
                        $setting['name'],
                        $setting['value'],
                        isset($setting['shopId']) ? "--shop {$setting['shopId']}" : ""
                    )
                );
            }
        }
    }
)->desc('Configure theme via deploy.php');

task(
    'kellerkinder:shopware:cache',
    function () {
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:cache:clear -q');
        run('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:theme:compile -q');

        if (get('warm_cache_after_deployment', false)) {
            run('cd {{shopware_public_path}} && {{bin/php}} bin/console sw:warm:http:cache -c -q');
        }
    }
)->desc('Clear Shopware cache and warm up HTTP cache');

task(
    'kellerkinder:shopware:opcache',
    function () {
        foreach (get('fcgi_sockets') as $socket) {
            run("cd {{release_path}} && if [ -f bin/cachetool.phar ]; then {{bin/php}} bin/cachetool.phar opcache:reset --fcgi={$socket}; else true; fi");
        }
    }
)->desc('Clear PHP OPcache');

task(
    'kellerkinder:shopware:staging',
    function () {
        run(
            'cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:plugin:install --activate K10rStaging'
        );
        $appConfig = get('app');
        run(
            sprintf('cd {{shopware_public_path}} && {{bin/php}} bin/console k10r:store:update --host %s --path %s --name %s --title %s',
                escapeshellarg(parse_url($appConfig["base_url"], PHP_URL_HOST)),
                escapeshellarg(get('shop_path')),
                escapeshellarg(get('shop_name')),
                escapeshellarg(get('shop_title'))
            )
        );
    }
)->desc('Set staging configuration');

task(
    'kellerkinder:shopware:production',
    function () {}
)->desc('Set production configuration');

/**
 * Main task
 */
task(
    'kellerkinder:shopware:install',
    [
        'kellerkinder:vendors',
    ]
)->desc('Download current Shopware distribution and copy it into deployed directory, install composer-based Shopware plugins');

task(
    'kellerkinder:shopware:configure',
    [
        'kellerkinder:shopware:opcache',
        'kellerkinder:shopware:plugins',
        'kellerkinder:shopware:config',
        'kellerkinder:shopware:cache',
    ]
)->desc('Install remaining Shopware plugins, set configuration values and clear cache');

before("deploy:prepare", "init");

task(
    'deploy:production',
    [
        'deploy:info',
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        "read-plugins-from-composer",
        'deploy:shared',
        'kellerkinder:shopware:install',
        'deploy:writable',
        'deploy:vendors',
        'deploy:clear_paths',
        'deploy:symlink',
        'kellerkinder:shopware:configure',
        'kellerkinder:shopware:production',
        'deploy:configure',
        'deploy:unlock',
        "reload-services",
        'cleanup',
        'success',
    ]
)->desc('Deploys a production shopware project');

task(
    'deploy:staging',
    [
        'deploy:info',
        'deploy:prepare',
        'deploy:lock',
        'deploy:release',
        'deploy:update_code',
        "read-plugins-from-composer",
        'deploy:shared',
        'kellerkinder:shopware:install',
        'deploy:writable',
        'deploy:vendors',
        'deploy:clear_paths',
        'deploy:symlink',
        'kellerkinder:shopware:configure',
        'kellerkinder:shopware:staging',
        'deploy:configure',
        'deploy:permissions',
        'deploy:unlock',
        "reload-services",
        'cleanup',
        'success',
    ]
)->desc('Deploys a staging shopware project');

task('deploy:permissions', function() {
    run("cd {{shopware_public_path}} && sudo chown -R " . get("http_user") . " var/ web/ Plugins/Local/Backend/RedMagnalister/ && sudo chmod -R 775 {{shopware_public_path}}/var/cache {{shopware_public_path}}/web/");
});

task('deploy:symlinkshopwaredocpath', function() {
    run("sudo rm -R {{shopware_public_path}}/vendor/shopware/shopware/files && rm -R {{shopware_public_path}}/vendor/shopware/shopware/media && sudo ln -s {{shopware_public_path}}/files {{shopware_public_path}}/vendor/shopware/shopware/files && sudo ln -s {{shopware_public_path}}/media {{shopware_public_path}}/vendor/shopware/shopware/media");
});

task('deploy:vcs:afterDeploy', function() {
    try {
        run("cd {{shopware_public_path}} && git add composer.lock && git commit -m \"current composer.lock after deployment\" && git push");
    } catch (\Exception $e) {
        if (!stristr($e->getMessage(), "no changes added to commit")) {
            writeln("<error>" . $e->getMessage() . "</error>");
        }
    }
});
before('deploy:permissions', 'deploy:vcs:afterDeploy');
after('deploy:symlink', 'deploy:symlinkshopwaredocpath');